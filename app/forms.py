from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SelectField, SelectMultipleField, SubmitField, widgets
from wtforms.validators import DataRequired, InputRequired
from app.pyscrypts.mongoDB import DB
from app.pyscrypts.load_git import u_id
import os


class CodeCheckerForm(FlaskForm):
    fullname = StringField('fullname в GitWork', validators=[DataRequired()])
    task = IntegerField('Задание', validators=[DataRequired('Введите целое число')])
    # task = SelectField('Задание', choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6)], coerce=int)
    # variant = SelectField('Вариант', choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10), (11, 11), (12, 12)], coerce=int)
    submit = SubmitField('Начать', id="default")
    rebuild_sub = SubmitField('Начать с перестроением матриц', id="rebuild")


class MultiCheckboxField(SelectMultipleField):
    """
    A multiple-select, except displays a list of checkboxes.

    Iterating the field will produce subfields, allowing custom rendering of
    the enclosed checkbox fields.
    """

    widget = widgets.ListWidget(prefix_label=False)
    option_widget = widgets.CheckboxInput()


def MakeSelectFilesForm():
    class SelectFilesForm(FlaskForm):
        pass
    SelectFilesForm.sub = SubmitField('Продолжить')
    selected_files = []
    i = 0
    for root, dirs, files in os.walk(os.getcwd() + '/projects_' + str(u_id) + '/', topdown=False):
        selected_files.append({'root': os.path.basename(root), 'files': files, 'varnames': [('dirname_' + str(i)), ('select_' + str(i))]})
        i += 1
    selected_files.pop(i - 1)

    for directory in selected_files:
        chosen_files = []
        for file in directory['files']:
            chosen_files.append((file, file))
        setattr(SelectFilesForm, directory['varnames'][0], directory['root'])
        setattr(SelectFilesForm, directory['varnames'][1], MultiCheckboxField(label="Файлы проекта "+ directory['root'], choices=chosen_files))

    return SelectFilesForm()
