import gitlab
import base64
import os
import json
from datetime import datetime
from .mongoDB import DB
import tempfile
import uuid

try:
    with open("/flask_example/app/pyscrypts/conf.json", "r") as read_file:
        data = json.load(read_file)
        stud_variants = data["stud_variants"]
        GITLAB_URL = data["GITLAB_URL"]
        ACCESS_TOKEN = data["ACCESS_TOKEN"]
        student_list = data["students"]
        subject = data["subject"]
except FileNotFoundError:
    print('Не найден файл конфигурации')
    exit(2)

# tmp = tempfile.TemporaryDirectory() # Временная директория, куда записываются проекты
u_id = uuid.uuid4().int


def Load_git(selected_student, task, rebuild):
    if not os.path.exists(os.getcwd() + '/projects_' + str(u_id) + '/'):
        os.mkdir(os.getcwd() + '/projects_' + str(u_id))

    for root, dirs, files in os.walk(os.getcwd() + '/projects_' + str(u_id) + '/', topdown=False): # Очистка директории со скачанными проектами
        for name in files:
            os.remove(os.path.join(root, name))
        for name in dirs:
            os.rmdir(os.path.join(root, name))

    if stud_variants.get(str(task)) is None:
        return -2


    gl = gitlab.Gitlab(GITLAB_URL, ACCESS_TOKEN)
    gl.auth()

    if selected_student not in [gl.projects.get(f'{s}/{subject}').owner.get('name') for s in student_list]:
        return -1

    task = str(task)
    variant = str(int(selected_student.split('-')[2]) % stud_variants[task] + 1)
    db = DB()
    var_matr_col = db["var matrices"]
    func_matr_col = db["func matrices"]
    check_student = db["check student"]
    # var_matr_col.delete_many({})
    # func_matr_col.delete_many({})
    check_student.delete_many({})
    check_student.insert_one({'student': selected_student, 'task': task, 'variant': variant})



    student_project = []

    without_changes = False # В целевой проект вносились/не вносились изменения
    for student in student_list:
        student_variant = (int(gl.projects.get(f'{student}/{subject}').owner.get('name').split('-')[2]) % stud_variants[
            task]) + 1
        if student_variant != int(variant):
            continue
        student_variant = str(student_variant)

        last_commit = gl.projects.get(f'{student}/{subject}').commits.list()[0].created_at
        year = int(last_commit.split('T')[0].split('-')[0])
        month = int(last_commit.split('T')[0].split('-')[1])
        day = int(last_commit.split('T')[0].split('-')[2])
        hour = int(last_commit.split('T')[1].split('.')[0].split(':')[0])
        minute = int(last_commit.split('T')[1].split('.')[0].split(':')[1])
        second = int(last_commit.split('T')[1].split('.')[0].split(':')[2])
        last_commit_date = datetime(year, month, day, hour, minute, second)

        if func_matr_col.find_one({'name': gl.projects.get(f'{student}/{subject}').owner.get('name'), 'task': task}) is None: # Если матрицы нет в базе
            func_matr_col.insert_one(
                {'name': gl.projects.get(f'{student}/{subject}').owner.get('name'), 'last_commit': last_commit_date,
                 'task': task, 'variant': student_variant, 'matrix': {}})
            var_matr_col.insert_one(
                {'name': gl.projects.get(f'{student}/{subject}').owner.get('name'), 'last_commit': last_commit_date,
                 'task': task, 'variant': student_variant, 'matrix': {}})
            student_project.append(gl.projects.get(f'{student}/{subject}'))
        elif rebuild:
            func_matr_col.update_one(
                {'name': gl.projects.get(f'{student}/{subject}').owner.get('name'),
                 'task': task}, {'$set':{'matrix': {}}})
            var_matr_col.update_one(
                {'name': gl.projects.get(f'{student}/{subject}').owner.get('name'),
                 'task': task}, {'$set': {'matrix': {}}})
            student_project.append(gl.projects.get(f'{student}/{subject}'))
        elif last_commit_date == func_matr_col.find_one({'name': gl.projects.get(f'{student}/{subject}').owner.get('name'), 'task': task}).get('last_commit'): # Если матрица есть и проект не изменялся
            if gl.projects.get(f'{student}/{subject}').owner.get('name') == selected_student:
                without_changes = True
            continue
        else: # Если матрица есть, но проект изменился
            func_matr_col.update_one(
                {'name': gl.projects.get(f'{student}/{subject}').owner.get('name'), 'task': task}, {'$set': {'last_commit': last_commit_date}})
            var_matr_col.update_one(
                {'name': gl.projects.get(f'{student}/{subject}').owner.get('name'), 'task': task},
                {'$set': {'last_commit': last_commit_date}})
            student_project.append(gl.projects.get(f'{student}/{subject}'))

    rep_with_task = {}
    for s_project in student_project:  # Получения проектов слушателей
        rep_with_task[s_project.owner['name']] = (s_project.repository_tree(path='task4-' + task + '/'))
    print(len(rep_with_task))

    if not without_changes:
        if len(rep_with_task.get(selected_student)) == 0:
            return -3
    try:
        archive_project = gl.projects.get(1887)
    except:
        archive_project_id = gl.projects.list(search='archive')[0].id
        archive_project = gl.projects.get(archive_project_id)
    arch_rep = {}
    for repository_1 in archive_project.repository_tree(path='task4-' + task + '/'):  # Получение проектов из архива
        if repository_1['type'] == 'tree' and int(repository_1.get('name')[2:]) == int(variant):
            for repository_2 in archive_project.repository_tree(repository_1.get('path')):
                if func_matr_col.find_one({'name': repository_2.get('name'), 'task': task}) is None:
                    arch_rep[repository_2.get('name')] = archive_project.repository_tree(repository_2.get('path'))
                    func_matr_col.insert_one({'name': repository_2.get('name'), 'last_commit': None, 'task': task, 'variant': variant, 'matrix': {}})
                    var_matr_col.insert_one({'name': repository_2.get('name'), 'last_commit': None, 'task': task, 'variant': variant, 'matrix': {}})
                elif rebuild:
                    arch_rep[repository_2.get('name')] = archive_project.repository_tree(repository_2.get('path'))
                    func_matr_col.update_one(
                        {'name': repository_2.get('name'), 'last_commit': None, 'task': task, 'variant': variant},
                        {"$set": {'matrix': {}}})
                    var_matr_col.update_one(
                        {'name': repository_2.get('name'), 'last_commit': None, 'task': task, 'variant': variant},
                        {"$set": {'matrix': {}}})

    # Create a students files
    i = 0
    for key, directory in rep_with_task.items():
        if not os.path.exists(os.getcwd() + '/projects_' + str(u_id) + '/' + key):
            os.mkdir(os.getcwd() + '/projects_' + str(u_id) + '/' + key)

        for j in range(len(directory)):
            if directory[j]['type'] == 'tree':
                continue
            file_info = student_project[i].repository_blob(directory[j]['id'])

            content = base64.b64decode(file_info['content'])
            with open(os.getcwd() + '/projects_' + str(u_id) + '/' + key + '/' + directory[j]['name'], 'w') as file_handler:
                try:
                    file_handler.write(content.decode("utf-8"))
                except UnicodeError as UE:
                    print(UE)
                    print(f'Failed to decode a file {file_handler.name}')
                    continue
        i += 1

    i = 0
    # Create a archive files
    for key, directory in arch_rep.items():
        if not os.path.exists(os.getcwd() + '/projects_' + str(u_id) + '/' + key):
            os.mkdir(os.getcwd() + '/projects_' + str(u_id) + '/' + key)
        for j in range(len(directory)):
            if directory[j]['type'] == 'tree':
                continue
            file_info = archive_project.repository_blob(directory[j]['id'])

            content = base64.b64decode(file_info['content'])
            with open(os.getcwd() + '/projects_' + str(u_id) + '/' + key + '/' + directory[j]['name'], 'w') as file_handler:
                try:
                    file_handler.write(content.decode("utf8"))
                except UnicodeError:
                    # print(f'Failed to decode a file {file_handler.name}')
                    os.remove(file_handler.name)
                    continue
        i += 1

    return [rep_with_task, arch_rep]
