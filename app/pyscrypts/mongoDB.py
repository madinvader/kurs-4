from pymongo import MongoClient


def DB():
    client = MongoClient('code_checker_DB', 27017)
    # client = MongoClient('localhost', 27017)

    db = client["matrices"]

    return db
