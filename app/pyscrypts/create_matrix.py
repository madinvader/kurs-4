import clang.cindex
import typing
import os
import copy
from .mongoDB import DB
from collections import OrderedDict
import ccsyspath
from .load_git import u_id

args = '-x c++ --std=c++17'.split()
syspath = ccsyspath.system_include_paths('clang++')
incargs = [ b'-I' + inc for inc in syspath ]
args = args + incargs
clang.cindex.Config.set_library_file('/usr/lib/x86_64-linux-gnu/libclang-6.0.so.1')


db = DB()
var_matr_col = db["var matrices"]
func_matr_col = db["func matrices"]
check_student = db["check student"]


def merge_dicts(dict1, dict2):
    for key in dict2.keys():
        if key in dict1.keys():
            dict1[key] += dict2[key]
        else:
            dict1[key] = dict2[key]


def filter_node_list_by_file(
        nodes: typing.Iterable[clang.cindex.Cursor],
        file_name: str
) -> typing.Iterable[clang.cindex.Cursor]:
    result = []

    for i in nodes:
        if os.path.dirname(os.path.abspath(i.location.file.name)) == os.path.dirname(os.path.abspath(file_name)):
            result.append(i)

    return result


def filter_node_list_by_node_kind(
        nodes: typing.Iterable[clang.cindex.Cursor],
        kinds: list
) -> typing.Iterable[clang.cindex.Cursor]:
    result = []

    for i in nodes:
        if i.kind in kinds:
            result.append(i)

    return result


def Create_matrix(student, selected_files):
    task = check_student.find_one({})['task']
    max_size = 0
    func_matrix = {} # Матрица функций
    var_matrix = {}  # Матрица переменных
    for file in selected_files:
        index = clang.cindex.Index.create()
        translation_unit = index.parse(os.getcwd() + '/projects_' + str(u_id) + '/' + student + '/' + file, args=args)
        source_nodes = filter_node_list_by_file(translation_unit.cursor.get_children(), translation_unit.spelling)
        all_functions = filter_node_list_by_node_kind(source_nodes, [clang.cindex.CursorKind.CXX_METHOD,
                                                                     clang.cindex.CursorKind.FUNCTION_DECL])
        all_classes = filter_node_list_by_node_kind(source_nodes, [clang.cindex.CursorKind.CLASS_DECL,
                                                                   clang.cindex.CursorKind.STRUCT_DECL,
                                                                   clang.cindex.CursorKind.CLASS_TEMPLATE])
        def find_func(
                all_func: list(),
                spell):
            for func in all_func:
                if func.spelling == spell:
                    return True
            return False
        # Дополняем список классов вложенными классами
        copy_all_classes = copy.copy(all_classes)
        for i in copy_all_classes:
            for node in i.get_children():
                if i.kind == clang.cindex.CursorKind.CLASS_DECL:
                    all_classes.append(node)
        all_variables_spelling = []  # Список имен переменых
        for i in all_classes:
            for node in i.get_children():
                if node.kind == clang.cindex.CursorKind.CXX_METHOD and find_func(all_functions,
                                                                                 node.spelling) is False:
                    all_functions.append(node)
                if node.kind == clang.cindex.CursorKind.FIELD_DECL and node.spelling != '':
                    all_variables_spelling.append(node.spelling)
        num_of_occ = {}  # Словарь вхождений функций в целевые функции
        for function in all_functions:
            num_of_occ[function.spelling] = 0
        num_of_occ_copy = num_of_occ.copy()
        for function in all_functions:
            for node in function.walk_preorder():
                if node.kind == clang.cindex.CursorKind.CALL_EXPR and node.spelling != '' and num_of_occ.get(
                        node.spelling) is not None:  # Если это вызов функции и она из списка пользовательских функций...
                    # print(f'{function.spelling}::{node.spelling} - {node.kind}')
                    num_of_occ_copy[
                        node.spelling] += 1  # ...то количество вхождений функции node.spelling увеличивается на 1
                if node.kind == clang.cindex.CursorKind.VAR_DECL and node.spelling != '':
                    all_variables_spelling.append(node.spelling)
            if func_matrix.get(
                    function.spelling) is not None:  # Если функция уже заполнена (например из другого файла)
                for key in num_of_occ_copy.keys():
                    if func_matrix[function.spelling].get(key) is None:
                        func_matrix[function.spelling][key] = num_of_occ_copy[key]
                    else:
                        func_matrix[function.spelling][key] += num_of_occ_copy[key]
            else:
                func_matrix[function.spelling] = num_of_occ_copy
            func_matrix[function.spelling] = dict(OrderedDict(sorted(func_matrix[function.spelling].items(), key=lambda t: t[1], reverse=True))) # Сортировка словарей в матрице функций
            num_of_occ_copy = num_of_occ.copy()
        for function in all_functions:
            num_of_var_list = []
            num_of_var = {}
            for node in function.walk_preorder():
                if (node.kind == clang.cindex.CursorKind.VAR_DECL or
                    ((
                             node.kind == clang.cindex.CursorKind.DECL_REF_EXPR or node.kind == clang.cindex.CursorKind.MEMBER_REF_EXPR)
                     and node.spelling in all_variables_spelling)) and node.spelling != '':
                    num_of_var_list.append(node.spelling)
            for var in set(num_of_var_list):
                num_of_var[var] = num_of_var_list.count(var)
            if var_matrix.get(function.spelling) is None:
                var_matrix[function.spelling] = {}
            merge_dicts(var_matrix[function.spelling], num_of_var) # Дополняем матрицу, построенную на предыдущем файле
            var_matrix[function.spelling] = dict(OrderedDict(sorted(var_matrix[function.spelling].items(), key=lambda t:t[1], reverse=True))) # Сортировка словарей в матрице переменных
    for d in var_matrix.values():
        if len(d) > max_size:
            max_size = len(d)

    def key_sorted(items):
        var_number = '0'
        counter = 0
        for i in items[1].values():
            counter += 1
            var_number += str(i)
            var_number += '0'
        for _ in range(max_size - counter):
            var_number += '00'
        return int(var_number)

    # Лексикографическая сортировка матрицы переменных
    func_matrix = dict(OrderedDict(sorted(func_matrix.items(), key=key_sorted, reverse=True)))
    var_matrix = dict(OrderedDict(sorted(var_matrix.items(), key=key_sorted, reverse=True)))

    var_matr_col.update_one({'name': student, 'task': task}, {'$set': {'matrix': var_matrix}})
    func_matr_col.update_one({'name': student, 'task': task}, {'$set': {'matrix': func_matrix}})

