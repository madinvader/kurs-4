from .mongoDB import DB

db = DB()
comparison = db["comparison"]

def get_func_variables(func_dict):
    func_variables = []
    for var in func_dict:
        func_variables.append(var)
    return func_variables


def Comparator():
    comparison.delete_many({})
    fullname = db["check student"].find_one({})['student']
    task = db["check student"].find_one({})['task']
    func_matrix = db["func matrices"].find_one({'name': fullname, 'task': task})
    var_matrix = db["var matrices"].find_one({'name': fullname, 'task': task})
    if func_matrix is None:
        exit(1)
    variant = db["check student"].find_one({})['variant']
    for file in db["func matrices"].find({'$and': [{'name': {'$ne': fullname}}, {'task': task}, {'variant': variant}]}):
        if comparison.find_one({'src': fullname, 'dst': file['name'], 'task': task, 'variant': variant}) is None:
            comparison.insert_one({'src': fullname, 'dst': file['name'], 'task': task, 'variant': variant})
        else:
            continue

    # Количество совпавших имен функций / минимальное кол-во функций из двух проектов
    same_func_name = 0 # Количество совпавших имен
    for file in db["func matrices"].find({'$and': [{'name': {'$ne':fullname}}, {'task': task}, {'variant': variant}]}):
        for func in func_matrix['matrix'].keys():
            juxtapose = func.maketrans({'_': None, '-': None}) # Таблица отображений (указываем, какие символы исключить из названий)
            if func.lower().translate(juxtapose) in map(lambda x: x.lower().translate(juxtapose), file['matrix'].keys()):
                same_func_name += 1
        try:
            comparison.update_one({'src': fullname, 'dst': file['name'], 'task': task, 'variant': variant}, {'$set': {'same_func_name': same_func_name / min(len(func_matrix['matrix'].keys()), len(file['matrix'].keys()))}})
        except ZeroDivisionError:
            comparison.update_one({'src': fullname, 'dst': file['name'], 'task': task, 'variant': variant}, {'$set': {'same_func_name': 0}})
        same_func_name = 0


    # Процент совпавших имен переменных в каждой функции
    same_var_name = 0 # Количество совпавших имен
    for file in db["var matrices"].find({'$and': [{'name': {'$ne':fullname}}, {'task': task}, {'variant': variant}]}):
        list_comparation = []
        i = -1
        for func in var_matrix['matrix'].keys():
            i += 1
            if (i + 1) > len(file['matrix'].values()):
                break
            func_variables = get_func_variables([*file['matrix'].values()][i].keys())
            for var in var_matrix['matrix'][func].keys():
                juxtapose = var.maketrans({'_': None, '-': None}) # Таблица отображений (указываем, какие символы исключить из названий)
                if var.lower().translate(juxtapose) in map(lambda x: x.lower().translate(juxtapose), func_variables):
                    same_var_name += 1
            try:
                list_comparation.append((func, [*file['matrix'].keys()][i],
                                         same_var_name / min(len([*var_matrix['matrix'].values()][i]),
                                                             len([*file['matrix'].values()][i]))))
            except ZeroDivisionError:
                list_comparation.append((func, [*file['matrix'].keys()][i], 0))
            same_var_name = 0
        comparison.update_one({'src': fullname, 'dst': file['name'], 'task': task, 'variant': variant},
                              {'$set': {'same_var_name': list_comparation}})

    # Сравнение матриц функций
    for file in db["func matrices"].find({'$and': [{'name': {'$ne':fullname}}, {'task': task}, {'variant': variant}]}): # Сравниваем с каждым слушателем с тем же заданием и вариантом
        src_un = []  # Разыменованная матрица функций выбранного слушателя
        src_matrix = [*func_matrix['matrix'].values()]
        unnamed_matrix = [] # Разыменованная матрица функций слушателя, с которым сравниваем
        dst_matrix = [*file['matrix'].values()]
        total = 0 # Общее число ненулевых значений в матрице
        consident = 0.0 # Сумма частных соответствующих элементов матриц
        for i in range(max(len(func_matrix['matrix']), len(file['matrix']))):
            unnamed_matrix.append([])
            src_un.append([])
            for j in range(max(len(func_matrix['matrix']), len(file['matrix']))):
                if (j + 1) > len(file['matrix']) or (i + 1) > len(file['matrix']):
                    unnamed_matrix[i].append(0)
                    src_un[i].append([*src_matrix[i].values()][j])
                elif (j + 1) > len(func_matrix['matrix']) or (i + 1) > len(func_matrix['matrix']):
                    src_un[i].append(0)
                    unnamed_matrix[i].append([*dst_matrix[i].values()][j])
                elif len([*dst_matrix[i].values()]) != len(file['matrix']):
                    unnamed_matrix[i].append(0)
                    src_un[i].append([*src_matrix[i].values()][j])
                elif len([*src_matrix[i].values()]) != len(func_matrix['matrix']):
                    src_un[i].append(0)
                    unnamed_matrix[i].append([*dst_matrix[i].values()][j])
                else:
                    unnamed_matrix[i].append([*dst_matrix[i].values()][j])
                    src_un[i].append([*src_matrix[i].values()][j])

                if src_un[i][j] != 0 or unnamed_matrix[i][j] != 0:
                    total += 1
                    try:
                        consident += min(src_un[i][j], unnamed_matrix[i][j]) / max(src_un[i][j], unnamed_matrix[i][j])
                    except ZeroDivisionError:
                        continue
        try:
            comparison.update_one({'src': fullname, 'dst': file['name'], 'task': task, 'variant': variant}, {'$set': {'compare_func_matrix': consident / total}})
        except ZeroDivisionError:
            comparison.update_one({'src': fullname, 'dst': file['name'], 'task': task, 'variant': variant}, {'$set': {'compare_func_matrix': 1}})
            continue

    # Сравниваем матрицы переменных
    for file in db["var matrices"].find({'$and': [{'name': {'$ne': fullname}}, {'task': task}, {'variant': variant}]}): # Сравниваем с каждым слушателем с тем же заданием и вариантом
        unnamed_var_matrix = [] # Разыменованная матрица переменых слушателя, с которым сравниваем
        dst_var_matrix = [*file['matrix'].values()]
        src_un_var = []  # Разыменованная матрица переменных выбранного слушателя
        src_var_matrix = [*var_matrix['matrix'].values()]

        # Выравниваем размер
        if len(dst_var_matrix) > len(src_var_matrix):
            for _ in range(abs(len(dst_var_matrix) - len(src_var_matrix))):
                src_var_matrix.append({})
        elif len(src_var_matrix) > len(dst_var_matrix):
            for _ in range(abs(len(dst_var_matrix) - len(src_var_matrix))):
                dst_var_matrix.append({})

        total = 0 # Общее число ненулевых значений в матрице
        consident = 0.0 # Сумма частных соответствующих элементов матриц
        for i in range(max(len(var_matrix['matrix']), len(file['matrix']))):
            unnamed_var_matrix.append([])
            src_un_var.append([])

            for j in range(max(len(src_var_matrix[i]), len(dst_var_matrix[i]))):
                if (j + 1) > len(dst_var_matrix[i]) or (i + 1) > len(file['matrix']):
                    unnamed_var_matrix[i].append(0)
                    src_un_var[i].append([*src_var_matrix[i].values()][j])
                elif (j + 1) > len(src_var_matrix[i]) or (i + 1) > len(var_matrix['matrix']):
                    src_un_var[i].append(0)
                    unnamed_var_matrix[i].append([*dst_var_matrix[i].values()][j])
                else:
                    unnamed_var_matrix[i].append([*dst_var_matrix[i].values()][j])
                    src_un_var[i].append([*src_var_matrix[i].values()][j])

                if src_un_var[i][j] != 0 or unnamed_var_matrix[i][j] != 0:
                    total += 1
                    try:
                        consident += min(src_un_var[i][j], unnamed_var_matrix[i][j]) / max(src_un_var[i][j], unnamed_var_matrix[i][j])
                    except ZeroDivisionError:
                        continue
        try:
            comparison.update_one({'src': fullname, 'dst': file['name'], 'task': task, 'variant': variant}, {'$set': {'compare_var_matrix': consident / total}})
        except ZeroDivisionError:
            comparison.update_one({'src': fullname, 'dst': file['name'], 'task': task, 'variant': variant}, {'$set': {'compare_var_matrix': 1}})
            continue

    # Сравниваем колличество функций
    for file in db["func matrices"].find({'$and': [{'name': {'$ne': fullname}}, {'task': task}, {'variant': variant}]}): # Сравниваем с каждым слушателем с тем же заданием и вариантом
        try:
            comparison.update_one({'src': fullname, 'dst': file['name'], 'task': task, 'variant': variant}, {'$set': {'comp_num_func': min(len(func_matrix['matrix'].keys()), len(file['matrix'].keys())) / max(len(func_matrix['matrix'].keys()), len(file['matrix'].keys()))}})
        except ZeroDivisionError:
            comparison.update_one({'src': fullname, 'dst': file['name'], 'task': task, 'variant': variant}, {'$set': {'comp_num_func': 0}})
    # Сравниваем количество переменных
    for file in db["var matrices"].find({'$and': [{'name': {'$ne':fullname}}, {'task': task}, {'variant': variant}]}):
        list_count = []
        i = -1
        for func in var_matrix['matrix'].values():
            i += 1
            if (i + 1) > len(file['matrix']):
                break
            try:
                list_count.append(([*var_matrix['matrix'].keys()][i], [*file['matrix'].keys()][i], min(len(func), len([*file['matrix'].values()][i])) / max(len(func), len([*file['matrix'].values()][i]))))
            except ZeroDivisionError:
                list_count.append(([*var_matrix['matrix'].keys()][i], [*file['matrix'].keys()][i], 0))
        comparison.update_one({'src': fullname, 'dst': file['name'], 'task': task, 'variant': variant}, {'$set': {'comp_num_var': list_count}})


