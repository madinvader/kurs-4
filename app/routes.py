# -*- coding: utf-8 -*-
from flask import render_template, flash, redirect, url_for, request
from app import app
from app.forms import CodeCheckerForm, MakeSelectFilesForm
from app.pyscrypts.load_git import Load_git, u_id
from app.pyscrypts.create_matrix import Create_matrix
from app.pyscrypts.comparator import Comparator
import os
import json
from app.pyscrypts.mongoDB import DB

db = DB()


@app.route('/code_checker', methods=['GET', 'POST'])
def code_checker():
    for root, dirs, files in os.walk(os.getcwd() + '/projects_' + str(u_id) + '/', topdown=False): # Очистка дериктории со скачанными проектами
        for name in files:
            os.remove(os.path.join(root, name))
        for name in dirs:
            os.rmdir(os.path.join(root, name))

    form = CodeCheckerForm(request.form)
    if form.validate_on_submit():
        if form.submit.data:
            lg = Load_git(selected_student=form.fullname.data, task=form.task.data, rebuild=False)
            print(lg)
            if lg == -2:
                flash(f'Нет задания под номером {form.task.data}')
                return redirect(url_for('code_checker'))
            if lg == -3:
                flash(f'У {form.fullname.data} нет задания №{form.task.data}')
                return redirect(url_for('code_checker'))
            if lg == -1:
                flash(f'Слушатель {form.fullname.data} не найден')
                return redirect(url_for('code_checker'))
        elif form.rebuild_sub.data:
            lg = Load_git(selected_student=form.fullname.data, task=form.task.data, rebuild=True)
            print(lg)
            if lg == -2:
                flash(f'Нет задания под номером {form.task.data}')
                return redirect(url_for('code_checker'))
            if lg == -3:
                flash(f'{form.fullname.data} не имеет задания №{form.task.data}')
                return redirect(url_for('code_checker'))
            if lg == -1:
                flash(f'Слушатель {form.fullname.data} не найден')
                return redirect(url_for('code_checker'))
        # flash('Задание {} fullname={}'.format(
        #     form.task.data, form.fullname.data))
        return redirect(url_for('file_selection'))
    print(form.errors)
    return render_template('code_checker.html', title='Code check', form=form)


@app.route('/code_checker/file_selection', methods=['GET', 'POST'])
def file_selection():
    form = MakeSelectFilesForm()
    selected_files = []
    i = 0
    for root, dirs, files in os.walk(os.getcwd() + '/projects_' + str(u_id) + '/', topdown=False):
        selected_files.append({'root': os.path.basename(root), 'files': files, 'varnames': [('dirname_' + str(i)), ('select_' + str(i))]})
        i += 1
    selected_files.pop(i - 1)
    if form.validate_on_submit():
        for directory in selected_files:
            # flash('Проект - {}, Выбор - {}'.format(getattr(form, directory['varnames'][0]), getattr(form, directory['varnames'][1]).data))
            Create_matrix(getattr(form, directory['varnames'][0]), getattr(form, directory['varnames'][1]).data)

        for root, dirs, files in os.walk(os.getcwd() + '/projects_' + str(u_id) + '/', topdown=False):
            for name in files:
                os.remove(os.path.join(root, name))
            for name in dirs:
                os.rmdir(os.path.join(root, name))

        return redirect(url_for('compare'))

    dirnames = []
    selects = []
    for directory in selected_files:
        dirnames.append(getattr(form, directory['varnames'][0]))
        selects.append(getattr(form, directory['varnames'][1]))
    if len(dirnames) == 0 and len(selects) == 0:
        return redirect(url_for('compare'))
    return render_template('file_selection.html', form=form, varnames=zip(dirnames, selects))


@app.route('/code_checker/file_selection/compare', methods=['GET', 'POST'])
def compare():
    Comparator()
    comparison = db["comparison"]

    results = []
    suspicious_res = []
    for result in comparison.find():
        try:
            if result['same_func_name'] + (sum([i[2] for i in result['same_var_name']]) / len(result['same_var_name'])) + 1.5 * result['compare_func_matrix'] + 2 * result['compare_var_matrix'] + 1.5 * result['comp_num_func'] + 1.5 * (sum([x[2] for x in result['comp_num_var']]) / len(result['comp_num_var'])) >= 5.5:
                suspicious_res.append(result)
            else:
                results.append(result)
        except ZeroDivisionError:
            results.append(result)
    return render_template('compare.html', suspicious_res=suspicious_res, results=results, src=comparison.find_one()['src'])
