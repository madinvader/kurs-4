FROM ubuntu:18.04

RUN apt-get update && apt-get install -y \
    python3.6 \
    python3-pip \
    python3-venv \
    clang \
    llvm

RUN apt-get install -y language-pack-en

ENV LANG=en_US.UTF-8

WORKDIR /flask_example

COPY requirements.txt requirements.txt
RUN python3 -m venv venv
RUN venv/bin/pip install --upgrade pip
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn

COPY app app
COPY node_modules node_modules
COPY code_checker.py config.py boot.sh ./

RUN chmod +x boot.sh

ENV FLASK_APP code_checker.py
ENV BOOTSTRAP_SERVE_LOCAL True

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]







